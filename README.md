Chat-Server
==============
Приложение состомт bp серверной и клиентской части, описание клиентской части смотрите README.md соответсвующего проекта.
Сервер состоит из следующих систем:
1. rabbitmq распределенный сервер очередей с поддержкой множества протоколов на прикладном уровне. в том числе http - прием и отправка сообщении
2. приложение - аутентификация пользователя, обработчка и отправка сообщении пользователю/пользователям
3. H2 БД - хранит информацию об авторизированных пользователей
4. Распределенный кэш Hazelcast - хранит топ последних сообщении и кэшированый запросы

Приложение можно запустить с ролью Slave, в этом случае нагрузка распределится между master и slave.  
Таким образом, мы имеем горизонтально масштабируемое решение, где каждая система может быть вынесена на отдельное железо.  
Система выдерживает свыше 1000 пользователей и отвечает заявленным требованиям  

###Ссылка на видео с тестом на 1000 пользователей
https://drive.google.com/file/d/0B_ad5vdRg1pzSTJfY2pQVTdfalE/view?usp=sharing

###Необходимое ПО
Все тесты проводились на ОС Windows 10, аналогичное можно сделать и на unix подобных системах
1. [Erlang OTP 20.1](http://erlang.org/download/otp_win64_20.1.exe "")
2. [rabbitmq-server-3.6.12](https://github.com/rabbitmq/rabbitmq-server/releases/download/rabbitmq_v3_6_12/rabbitmq-server-3.6.12.exe "")
После установки и успешного запуска, в вэб браузере можно открыть админскую панель по ссылке **http://[host]:15672**, логин/пароль по умолчанию guest/guest
![pic1.png](image/pic3.png "")
3. Java 8

###Параметры запуска сервера в режиме:
#####Master
######Параметры
-ea -Dapp.config.cache.client.address=127.0.0.2 -Dapp.mode=master -Dspring.rabbitmq.host=127.0.0.1 -Dspring.rabbitmq.username=guest -Dspring.rabbitmq.password=guest -Dspring.datasource.url=jdbc:h2:tcp://localhost:9092/mem:testdb -Dspring.datasource.password= -Dspring.datasource.username=sa -Dspring.application.name=master -Dserver.port=8080
######Из командной строки
java -jar -ea -Dapp.config.cache.client.address=127.0.0.2 -Dapp.mode=master -Dspring.rabbitmq.host=127.0.0.1 -Dspring.rabbitmq.username=guest -Dspring.rabbitmq.password=guest -Dspring.datasource.url=jdbc:h2:tcp://localhost:9092/mem:testdb -Dspring.datasource.password= -Dspring.datasource.username=sa -Dspring.application.name=master -Dserver.port=8080 chat-server-1.0-SNAPSHOT.jar
#####Slave
######Параметры
-ea -Dapp.config.cache.client.address=127.0.0.2 -Dapp.mode=slave -Dspring.rabbitmq.host=127.0.0.1 -Dspring.rabbitmq.username=guest -Dspring.rabbitmq.password=guest -Dspring.datasource.url=jdbc:h2:tcp://localhost:9092/mem:testdb -Dspring.datasource.password= -Dspring.datasource.username=sa -Dspring.application.name=slave -Dserver.port=8081
######Из командной строки
java -jar -ea -Dapp.config.cache.client.address=127.0.0.2 -Dapp.mode=slave -Dspring.rabbitmq.host=127.0.0.1 -Dspring.rabbitmq.username=guest -Dspring.rabbitmq.password=guest -Dspring.datasource.url=jdbc:h2:tcp://localhost:9092/mem:testdb -Dspring.datasource.password= -Dspring.datasource.username=sa -Dspring.application.name=slave -Dserver.port=8081 chat-server-1.0-SNAPSHOT.jar

Полсе запуска сервера, можно запускать клиентов  

####Схема передачи сообщении от пользователя к серверу
![pic1.png](image/pic1.png "")
####Схема передачи сообщении от приложения к пользователю
![pic1.png](image/pic2.png "")