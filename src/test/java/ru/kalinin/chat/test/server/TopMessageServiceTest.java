package ru.kalinin.chat.test.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.server.Application;
import ru.kalinin.chat.server.Profiles;
import ru.kalinin.chat.server.service.topmessage.TopMessageService;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * Created by kalinin on 03.10.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.TEST)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes=Application.class)
public class TopMessageServiceTest {

    @Autowired
    private TopMessageService topMessageService;

    @Value(value = "${app.config.topLatestMessages}")
    private Integer topLatestMessages;

    @Test
    public void fillLastMessages() {

        Message m1 = new Message();
        m1.setFrom("-101");
        m1.setBody("Сообщение 1");
        topMessageService.add(m1);

        Message m2 = new Message();
        m2.setFrom("-102");
        m2.setBody("Сообщение 2");
        topMessageService.add(m2);

        Message m3 = new Message();
        m3.setFrom("-103");
        m3.setBody("Сообщение 3");
        topMessageService.add(m3);

        Message m4 = new Message();
        m4.setFrom("-104");
        m4.setBody("Сообщение 4");
        topMessageService.add(m4);

        assertEquals(topMessageService.getLast().size(), 4);
    }
}
