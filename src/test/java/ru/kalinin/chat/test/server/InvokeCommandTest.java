package ru.kalinin.chat.test.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kalinin.chat.server.Application;
import ru.kalinin.chat.server.Profiles;
import ru.kalinin.chat.server.component.command.CommandHelper;
import ru.kalinin.chat.server.component.command.invoke.CommandInvoke;
import ru.kalinin.chat.server.component.command.invoke.DateCommand;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * Created by kalinin on 03.10.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.TEST)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes=Application.class)
public class InvokeCommandTest {

    @Autowired
    private CommandHelper commandHelper;

    @Test
    public void invokeCommand() {

        DateCommand dateCommandSend = new DateCommand();
        CommandInvoke commandReceive = commandHelper.lookup(dateCommandSend.getName());
        assertEquals(commandReceive.getName(), dateCommandSend.getName());
    }
}
