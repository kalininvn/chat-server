package ru.kalinin.chat.test.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kalinin.chat.server.Application;
import ru.kalinin.chat.server.Profiles;
import ru.kalinin.chat.server.model.client.AuthUserImpl;
import ru.kalinin.chat.server.service.broker.BrokerUserManager;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


/**
 * Created by kalinin on 29.09.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.TEST)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes=Application.class)
public class UserDetailsManagerImplTest {


    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UserDetailsManager userDetailsManager;

    @Autowired
    private BrokerUserManager brokerUserManager;

    @Test
    public void whenAuthUser_thenUserCreated() throws Exception {

        ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/api/authenitcation/createuser", AuthUserImpl.builder().username("vova").build(), Void.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        responseEntity = restTemplate.postForEntity("/api/authenitcation/createuser", AuthUserImpl.builder().username("vova").build(), Void.class);
        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        Integer userId = userDetailsManager.getUserIdByUsername("vova");
        userDetailsManager.getUserIdByUsername("vova");
        userDetailsManager.getUserIdByUsername("vova");
        userDetailsManager.userExists("vova");
        userDetailsManager.userExists("vova");
        userDetailsManager.logoutAll();
        userDetailsManager.userExists("vova");
//        userDetailsManager.getUserIdByUsername("vova");
    }
}
