package ru.kalinin.chat.test.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kalinin.chat.commons.model.packet.Command;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.server.Application;
import ru.kalinin.chat.server.Profiles;
import ru.kalinin.chat.server.helper.MessageHelper;
import ru.kalinin.chat.server.model.client.AmqpConfiguration;
import ru.kalinin.chat.server.model.client.AuthUserImpl;
import ru.kalinin.chat.server.model.security.UserDetails;
import ru.kalinin.chat.server.service.broker.BrokerUserManager;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * Created by kalinin on 29.09.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.TEST)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes=Application.class)
public class BrokerUserManagerImplTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitAdmin adminAmqp;

    @Autowired
    private BrokerUserManager brokerUserManager;

    @Autowired
    private UserDetailsManager userDetailsManager;

    @Autowired
    private MessageHelper messageHelper;

    private AmqpConfiguration amqpConfiguration1 = null;
    private Integer userIdClient1 = null;
    private AmqpConfiguration amqpConfiguration2 = null;
    private Integer userIdClient2 = null;

    @Before
    public void setUp() {


        UserDetails userDetails = userDetailsManager.createUser(AuthUserImpl.builder().username("vova").build());
        amqpConfiguration1 = brokerUserManager.createAndBind(userDetails.getId());
        userIdClient1 = userDetails.getId();

        userDetails = userDetailsManager.createUser(AuthUserImpl.builder().username("valera").build());
        amqpConfiguration2 = brokerUserManager.createAndBind(userDetails.getId());
        userIdClient2 = userDetails.getId();
    }

    @Test
    public void whenMessageIncomingProducer_thenMessageIncomingConsumers() throws Exception {

        // отправляем сообщение
        Message mr = new Message();
        mr.setFrom(userDetailsManager.getUsernameByUserId(userIdClient1));
        mr.setBody("Сообщение-" + 0);
        // отправляем сообщение серверу
        rabbitTemplate.convertAndSend(messageHelper.getExchangeNameMessageIncoming(), messageHelper.getRoutingNameMessageIncoming(),  mr);
        Thread.sleep(2000);
        // ждем ответа
        org.springframework.amqp.core.Message message = rabbitTemplate.receive(amqpConfiguration1.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        assertNotNull(message);

        // ждем ответа
        message = rabbitTemplate.receive(amqpConfiguration2.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        assertNotNull(message);
    }

    @After
    public void tearDown() {
        adminAmqp.deleteQueue(amqpConfiguration1.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        userDetailsManager.deleteUserByUsername(userDetailsManager.getUsernameByUserId(userIdClient1));
        adminAmqp.deleteQueue(amqpConfiguration2.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        userDetailsManager.deleteUserByUsername(userDetailsManager.getUsernameByUserId(userIdClient2));
    }

    @Test
    public void whenMessageOutgoingProducer_thenMessageIncomingConsumers() throws Exception {

        // отправляем сообщение
        Command cmd = new Command();
        cmd.setCode("date");
        cmd.setArgs("arg1", "arg2");
        cmd.setFrom(userDetailsManager.getUsernameByUserId(userIdClient1));
        // отправляем сообщение серверу
        rabbitTemplate.convertAndSend(messageHelper.getExchangeNameMessageIncoming(), messageHelper.getRoutingNameMessageIncoming(),  cmd);
        Thread.sleep(2000);
        // ждем ответа
        org.springframework.amqp.core.Message message = rabbitTemplate.receive(amqpConfiguration1.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        assertNotNull(message);

        // ждем ответа
        message = rabbitTemplate.receive(amqpConfiguration2.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        assertNull(message);
    }
}
