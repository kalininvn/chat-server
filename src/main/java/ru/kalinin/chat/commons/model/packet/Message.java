package ru.kalinin.chat.commons.model.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kalinin on 04.10.2017.
 */
@Getter @Setter
public class Message extends Packet implements Serializable {

    private final Set<Body> bodies = new HashSet<Body>();
    private Type type = Type.NORMAL;

    public Message() {
    }

    public Message(Type type) {
        this.type = type;
    }

    public Message(String body) {
        addBody(body);
    }

    public Message(String body, Type type) {
        this(body);
        this.type = type;
    }

    @Override
    public String getTypePacket() {
        return "message";
    }

    public enum Type {
        NORMAL,
        GROUPCHAT;
    }

    public void setBody(String body) {
        bodies.clear();
        addBody(body);
    }

    public Body addBody(String body) {
        if(body == null) {
            body = "";
        }
        Body messageBody = new Body(body);
        bodies.add(messageBody);
        return messageBody;
    }

    public String getBody() {
        for (Body body : bodies) {
            return body.message;
        }
        return null;
    }


    @AllArgsConstructor
    @NoArgsConstructor
    @Getter @Setter
    public static class Body implements Serializable {
        String message;
    }
}
