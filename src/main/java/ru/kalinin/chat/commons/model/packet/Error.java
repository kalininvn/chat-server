package ru.kalinin.chat.commons.model.packet;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by kalinin on 04.10.2017.
 */
@Getter
@Setter
public class Error extends Packet implements Serializable {

    private Type type;
    private String body;

    public Error() {
    }

    public Error(Type type, String message) {
        this.type = type;
        this.body = message;
    }

    @Override
    public String getTypePacket() {
        return "error";
    }

    public static enum Type {
        CANCEL,
        AUTH_INVALID
    }
}
