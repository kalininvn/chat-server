package ru.kalinin.chat.commons.model.packet;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by kalinin on 29.09.2017.
 */
public class Command extends Packet implements Serializable  {
    @Getter
    @Setter
    private String code;
    @Getter
    private String[] args;

    public Command() {
    }

    public Command(String code, String[] args) {
        this.code = code;
        this.args = args;
    }

    @Override
    public String getTypePacket() {
        return "command";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String... args) {
        this.args = args;
    }
}
