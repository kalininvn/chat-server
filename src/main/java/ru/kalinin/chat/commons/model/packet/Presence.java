package ru.kalinin.chat.commons.model.packet;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by kalinin on 05.10.2017.
 */
@Getter
@Setter
public class Presence extends Packet implements Serializable {

    private Type type = Type.AVAILABLE;

    public Presence() {
    }

    public Presence(Type type) {
        this.type = type;
    }

    @Override
    public String getTypePacket() {
        return "presence";
    }


    public enum Type {
        AVAILABLE,
        UNAVAILABLE
    }
}
