package ru.kalinin.chat.server.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.commons.model.packet.*;
import ru.kalinin.chat.commons.model.packet.Error;
import ru.kalinin.chat.server.Constant;
import ru.kalinin.chat.server.component.command.CommandHelper;
import ru.kalinin.chat.server.component.command.CommandInvokeStatus;
import ru.kalinin.chat.server.component.command.CommandResult;
import ru.kalinin.chat.server.component.command.invoke.CommandInvoke;
import ru.kalinin.chat.server.producer.MessageOutgoingProducer;
import ru.kalinin.chat.server.service.security.UserDetailsManager;
import ru.kalinin.chat.server.service.topmessage.TopMessageService;

/**
 * Created by kalinin on 30.09.2017.
 */
@Slf4j
@Component
public class MessageIncomingConsumerImpl implements MessageIncomingConsumer {

    @Value("${spring.application.name}")
    private String appName;
    @Autowired
    private TopMessageService<Message> topMessageService;
    @Autowired
    private UserDetailsManager userDetailsManager;
    @Autowired
    private CommandHelper commandHelper;
    @Autowired
    private MessageOutgoingProducer messageOutgoingProducer;

    @RabbitListener(queues = {Constant.QUEUE_MESSAGE_INCOMING})
    @Override
    public void receiveMessage(Packet packet) {
        log.info("Принято сообщение приложением \"" + appName + "\" от пользователя \"" + packet.getFrom() + "\"");

        if(packet == null) {
            return;
        }

        try {
            if(!userDetailsManager.userExists(packet.getFrom())) {
                return;
            }

            if(packet instanceof Command) {
                processCommand((Command) packet);
            }
            else if(packet instanceof Presence) {
                processPresence((Presence) packet);
            }
            else if(packet instanceof Message) {
                processMessage((Message) packet);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void processCommand(Command command) {
        CommandResponse response = new CommandResponse();
        response.setFrom(Constant.USER_NAME_SERVER + appName);
        response.setTo(command.getFrom());
        String code = command.getCode();

        try {
            CommandInvoke invokeInstance = commandHelper.lookup(code);
            CommandResult cr = null;
            if(invokeInstance == null) {
                response.setBody("Команда \"" + code + "\" не найдена");
                response.setStatus(CommandResponse.Status.NOT_FOUND);
            } else {
                invokeInstance.setArgs(command.getArgs());
                invokeInstance.setFrom(command.getFrom());

                cr = invokeInstance.invoke();
            }
            if(cr == null) {
                response.setBody("Команда \"" + code + "\" не найдена");
                response.setStatus(CommandResponse.Status.NOT_FOUND);
            } else if(cr.getStatus() != null && cr.getStatus() == CommandInvokeStatus.SUCCESS) {
                response.setBody(cr.getMessage());
                response.setStatus(CommandResponse.Status.SUCCESS);
            }
            else {
                response.setBody("Ошибка при выполнении команды \"" + code + "\" с параметрами \"" + String.join(",", command.getArgs()) + "\"");
                response.setStatus(CommandResponse.Status.ERROR);
            }
        } catch (Exception e) {
            response.setBody("Ошибка при выполнении команды \"" + code + "\" с параметрами \"" + String.join(",", command.getArgs()) + "\"");
            response.setStatus(CommandResponse.Status.ERROR);
            e.printStackTrace();
        }

        messageOutgoingProducer.sendUser(response.getTo(), response);
    }

    private void processError(String to, String errorMessage) {
        Error response = new Error();
        response.setFrom(Constant.USER_NAME_SERVER + appName);
        response.setTo(to);
        response.setBody(errorMessage);
        response.setType(Error.Type.AUTH_INVALID);
        messageOutgoingProducer.sendUser(response.getTo(), response);
    }

    private void processMessage(Message message) {
        Message response = new Message();
        response.setFrom(message.getFrom());
        response.setBody(message.getBody());
        topMessageService.add(response);
        messageOutgoingProducer.sendUserAll(response);
    }

    private void processPresence(Presence presence) {
        Presence response = new Presence(presence.getType());
        response.setFrom(presence.getFrom());
        if(presence.getType() == Presence.Type.UNAVAILABLE) {
            userDetailsManager.logout(presence.getFrom());
        }
        messageOutgoingProducer.sendUserAll(response);
    }
}
