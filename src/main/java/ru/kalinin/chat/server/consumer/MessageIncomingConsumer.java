package ru.kalinin.chat.server.consumer;

import ru.kalinin.chat.commons.model.packet.Packet;

/**
 * Created by kalinin on 04.10.2017.
 */
public interface MessageIncomingConsumer {

    void receiveMessage(Packet packet);
}
