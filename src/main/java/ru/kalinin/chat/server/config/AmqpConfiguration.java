package ru.kalinin.chat.server.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.kalinin.chat.server.Constant;
import ru.kalinin.chat.server.AppMode;

/**
 * Created by kalinin on 29.09.2017.
 */
@Configuration
public class AmqpConfiguration {

    @Value(value = "${app.config.cache.client.address}")
    private String clientAddress;

    @Value(value = "${app.mode}")
    private String appMode;

    /**
     * Очередь для всех входящих сообщении серверу
     * @return
     */
    @Bean
    Queue queueMessageIncoming() {
        return new Queue(Constant.QUEUE_MESSAGE_INCOMING, false);
    }

    /**
     * точка доступа для всех входящих сообщении серверу
     * @return
     */
    @Bean
    DirectExchange exchangeMessageIncoming() {
        return new DirectExchange(Constant.EXCHANGE_MESSAGE_INCOMING);
    }

    /**
     * точка доступа ответов на запрос пользователя, в нашем случае команд
     * @return
     */
    @Bean
    TopicExchange exchangeMessageOutgoing() {
        return new TopicExchange(Constant.EXCHANGE_MESSAGE_OUTGOING);
    }

    @Bean
    RabbitAdmin adminAmqp(ConnectionFactory connectionFactory
            , DirectExchange exchangeMessageIncoming, Queue queueMessageIncoming
            , TopicExchange exchangeMessageOutgoing) {

        RabbitAdmin adminAmqp = new RabbitAdmin(connectionFactory);
        if(appMode.equals(AppMode.MASTER)) {
            adminAmqp.declareExchange(exchangeMessageIncoming);
            adminAmqp.declareExchange(exchangeMessageOutgoing);
            adminAmqp.deleteQueue(queueMessageIncoming.getName());
            adminAmqp.declareQueue(queueMessageIncoming);
            adminAmqp.declareBinding(BindingBuilder.bind(queueMessageIncoming).to(exchangeMessageIncoming).with(Constant.ROUTING_MESSAGE_INCOMING));
        }
        return adminAmqp;
    }
}
