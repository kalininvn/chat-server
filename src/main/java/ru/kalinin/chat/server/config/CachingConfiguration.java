package ru.kalinin.chat.server.config;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.RingbufferConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.ringbuffer.Ringbuffer;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.AppMode;
import ru.kalinin.chat.server.Constant;
import ru.kalinin.chat.server.component.cache.CacheChatManager;
import ru.kalinin.chat.server.component.cache.HazelcastCacheChatManager;

/**
 * Created by kalinin on 02.10.2017.
 */

@Slf4j
@Configuration
@EnableCaching
public class CachingConfiguration {

    @Value(value = "${app.config.topLatestMessages}")
    private Integer topLatestMessages;

    @Value(value = "${app.config.cache.client.address}")
    private String clientAddress;

    @Value(value = "${app.mode}")
    private String appMode;

    @Bean
    KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }

    @Bean
    @Order(1)
    public HazelcastInstance hazelcastInstanceServer() {
        if(appMode.equals(AppMode.SLAVE)) {
            return null;
        }
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        initConfigTopLatestMessages(hazelcastInstance);
        return hazelcastInstance;
    }

    @Bean
    @Order(2)
    HazelcastInstance hazelcastInstanceClient() {
        // for client HazelcastInstance LocalMapStatistics will not AVAILABLE
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress(clientAddress);
        HazelcastInstance hazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
        return hazelcastInstance;
    }

    @DependsOn({"hazelcastInstanceServer"})
    @Bean
    CacheChatManager cacheManager(HazelcastInstance hazelcastInstanceClient) {
        return new HazelcastCacheChatManager(hazelcastInstanceClient);
    }

    private void initConfigTopLatestMessages(HazelcastInstance hazelcastInstanceServer) {
        RingbufferConfig tlmRingbufferConfig = new RingbufferConfig(Constant.CACHE_NAME_TOP_LATEST_MESSAGES);
        tlmRingbufferConfig.setCapacity(topLatestMessages);
        hazelcastInstanceServer.getConfig().addRingBufferConfig(tlmRingbufferConfig);
    }
}