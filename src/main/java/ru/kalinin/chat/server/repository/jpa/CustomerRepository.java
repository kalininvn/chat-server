package ru.kalinin.chat.server.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import ru.kalinin.chat.server.model.jpa.Customer;

import java.util.List;

/**
 * Created by kalinin on 01.10.2017.
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    int countByName(String name);
    Customer findByName(String username);
    void deleteByName(String username);
}
