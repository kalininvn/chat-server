package ru.kalinin.chat.server.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.commons.model.packet.Packet;
import ru.kalinin.chat.server.helper.MessageHelper;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

/**
 * Created by kalinin on 04.10.2017.
 */
@Component
public class MessageOutgoingProducerImpl implements MessageOutgoingProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MessageHelper messageHelper;

    @Autowired
    private UserDetailsManager userDetailsManager;

    @Override
    public void sendUser(String username, Packet packet) {
        Integer userId = userDetailsManager.getUserIdByUsername(username);
        rabbitTemplate.convertAndSend(messageHelper.getExchangeNameMessageOutgoing(), messageHelper.getRoutingNameMessageOutgoing(userId), packet);
    }

    @Override
    public void sendUserAll(Packet packet) {
        rabbitTemplate.convertAndSend(messageHelper.getExchangeNameMessageOutgoing(), messageHelper.getRoutingNameMessageOutgoingAll(), packet);
    }
}
