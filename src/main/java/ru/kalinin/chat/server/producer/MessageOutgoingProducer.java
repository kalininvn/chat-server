package ru.kalinin.chat.server.producer;

import ru.kalinin.chat.commons.model.packet.Packet;

/**
 * Created by kalinin on 04.10.2017.
 */
public interface MessageOutgoingProducer {

    void sendUser(String username, Packet packet);
    void sendUserAll(Packet packet);
}
