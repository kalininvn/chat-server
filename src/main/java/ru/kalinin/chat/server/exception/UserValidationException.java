package ru.kalinin.chat.server.exception;

/**
 * Created by kalinin on 10.10.2017.
 */
public class UserValidationException extends RuntimeException {

    public UserValidationException() {
    }

    public UserValidationException(String message) {
        super(message);
    }
}
