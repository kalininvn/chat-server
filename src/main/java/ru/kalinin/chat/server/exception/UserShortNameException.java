package ru.kalinin.chat.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 10.10.2017.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class UserShortNameException extends UserValidationException {

    public UserShortNameException() {
        super("Имя не должно быть меньше 3 символов");
    }

    public UserShortNameException(String message) {
        super(message);
    }
}
