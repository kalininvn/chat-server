package ru.kalinin.chat.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 10.10.2017.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class UserLongNameException extends UserValidationException {

    public UserLongNameException() {
        super("Имя не должно превышать 20 символов");
    }

    public UserLongNameException(String message) {
        super(message);
    }
}
