package ru.kalinin.chat.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 29.09.2017.
 */
@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Ошибка аутентификации")
public class AuthException extends RuntimeException {
}
