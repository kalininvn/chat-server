package ru.kalinin.chat.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by kalinin on 29.09.2017.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN)
public class UserIsExistsException extends UserValidationException {

    public UserIsExistsException() {
        super("Пользователь с таким именем существует");
    }

    public UserIsExistsException(String message) {
        super(message);
    }
}
