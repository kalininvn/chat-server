package ru.kalinin.chat.server.component.command.invoke;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.component.command.CommandInvokeStatus;
import ru.kalinin.chat.server.component.command.CommandResult;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

/**
 * Created by kalinin on 11.10.2017.
 */
@Component
public class UserCommand extends AbstractCommandInvoke {

    @Autowired
    private UserDetailsManager userDetailsManager;

    @Override
    public String getHelp() {
        return "получить id текущего пользователя";
    }

    @Override
    public String getName() {
        return "user";
    }

    @Override
    public void setArgs(String... arg) {
    }

    @Override
    public CommandResult invoke() {
        return CommandResult.builder()
                .status(CommandInvokeStatus.SUCCESS)
                .message("Ваш ид " + userDetailsManager.getUserIdByUsername(getFrom()))
                .build();
    }
}
