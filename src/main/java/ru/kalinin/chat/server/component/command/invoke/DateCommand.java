package ru.kalinin.chat.server.component.command.invoke;

import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.component.command.CommandInvokeStatus;
import ru.kalinin.chat.server.component.command.CommandResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * Created by kalinin on 03.10.2017.
 */
@Component
public class DateCommand extends AbstractCommandInvoke {

    private static final String COMMAND_NAME = "date";
    private DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    @Override
    public String getHelp() {
        return "текущая дата";
    }

    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @Override
    public CommandResult invoke() {
        return CommandResult.builder()
                .message("Текущая дата " + dateFormat.format(GregorianCalendar.getInstance().getTime()))
                .status(CommandInvokeStatus.SUCCESS)
                .build();

    }
}