package ru.kalinin.chat.server.component.command;

import lombok.*;

/**
 * Created by kalinin on 03.10.2017.
 */
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommandResult {

    private CommandInvokeStatus status;
    private String message;
}
