package ru.kalinin.chat.server.component.cache;

import com.google.common.util.concurrent.Futures;
import com.hazelcast.config.RingbufferConfig;
import com.hazelcast.core.DistributedObjectEvent;
import com.hazelcast.core.DistributedObjectListener;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.ringbuffer.ReadResultSet;
import com.hazelcast.ringbuffer.Ringbuffer;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by kalinin on 03.10.2017.
 */
public class HazelcastCacheChatManager extends HazelcastCacheManager implements CacheChatManager {

    public HazelcastCacheChatManager() {
        super();
    }

    public HazelcastCacheChatManager(HazelcastInstance hazelcastInstanceClient) {
        super(hazelcastInstanceClient);
    }

    @Override
    public <E> void addRingbuffer(String name, E object) {
        Ringbuffer<E> rb = getHazelcastInstance().getRingbuffer(name);
        rb.add(object);
    }

    @Override
    @SneakyThrows
    public <E> List<E> getRingbuffer(String name, Class<E> type) {
        return getRingbuffer(name, -1, type);
    }

    @Override
    @SneakyThrows
    public <E> List<E> getRingbuffer(String name, int maxCount, Class<E> type) {
        if(maxCount != -1 && maxCount<= 0) {
            throw new IllegalArgumentException();
        }

        Ringbuffer<E> rb = getHazelcastInstance().getRingbuffer(name);
        if(maxCount > 0) {
            maxCount = (maxCount > rb.size()) ? (int) rb.size() : maxCount;
        } else {
            maxCount = (int) rb.size();
        }

        ICompletableFuture<ReadResultSet<E>> response =  rb.readManyAsync(rb.headSequence(), 0, maxCount, input -> true);
        System.out.println(response);

        ReadResultSet<E> payload = Futures.getUnchecked(response);
        return StreamSupport.stream(payload.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Cache getCache(String name) {
        return super.getCache(name);
    }

    @Override
    public Collection<String> getCacheNames() {
        return super.getCacheNames();
    }
}
