package ru.kalinin.chat.server.component.command;

/**
 * Created by kalinin on 03.10.2017.
 */
public enum CommandInvokeStatus {
    SUCCESS, ERROR
}
