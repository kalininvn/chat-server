package ru.kalinin.chat.server.component.command.invoke;

import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.component.command.CommandHelper;
import ru.kalinin.chat.server.component.command.CommandInvokeStatus;
import ru.kalinin.chat.server.component.command.CommandResult;

/**
 * Created by kalinin on 03.10.2017.
 */
@Component
public class HelpCommand implements CommandInvoke {

    private static final String COMMAND_NAME = "help";

    @Override
    public void setFrom(String from) {

    }

    @Override
    public String getFrom() {
        return null;
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @Override
    public void setArgs(String... args) {

    }

    @Override
    public CommandResult invoke() {
        StringBuilder info = new StringBuilder();
        info.append(getMessage("Это справочна я информация по командам"));

        for (Class<CommandInvoke> aClass : CommandHelper.name2class.values()) {
            try {
                String help = aClass.newInstance().getHelp();
                String name = aClass.newInstance().getName();
                if(COMMAND_NAME.equals(name)) {
                    continue;
                }
                info.append(getMessage("/" + name + " - " + help));
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }


        }
        info.append(getMessage("/shutdown - гасит терминал, обработчик на стороне клиента"));
        CommandResult result = new CommandResult(CommandInvokeStatus.SUCCESS, info.toString());
        return result;
    }

    private String getMessage(String message) {
        return message + "\n";
    }
}
