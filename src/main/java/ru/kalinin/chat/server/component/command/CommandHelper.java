package ru.kalinin.chat.server.component.command;

/**
 * Created by kalinin on 03.10.2017.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.component.command.invoke.AbstractCommandInvoke;
import ru.kalinin.chat.server.component.command.invoke.CommandInvoke;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CommandHelper {

    public static Map<String, Class<CommandInvoke>> name2class = new HashMap<>();
    @Autowired
    ApplicationContext applicationContext;

    static {
        List<? extends Class<CommandInvoke>> classes = findClasses();
        mapClasses(classes);
    }

    public CommandInvoke lookup(String commandName) {
        if(name2class.containsKey(commandName)) {
           Class<CommandInvoke> findClass = name2class.get(commandName);
            return applicationContext.getBean(findClass);
        } else {
            return null;
        }
    }

    private static List<? extends Class<CommandInvoke>> findClasses() {

        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter((metadataReader, metadataReaderFactory) ->
                Arrays.asList(metadataReader.getClassMetadata().getInterfaceNames()).stream()
                        .anyMatch(
                                aClass -> aClass.contains(CommandInvoke.class.getName())
                        )
                        ||
                        (
                                metadataReader.getClassMetadata().getSuperClassName() != null && metadataReader.getClassMetadata().getSuperClassName().contains(AbstractCommandInvoke.class.getCanonicalName())
                        )

        );
        final Set<BeanDefinition> components = scanner
                .findCandidateComponents(CommandInvoke.class.getPackage().getName());

        return components.stream().map(
                beanDefinition -> {
                    try {
                        return (Class<CommandInvoke>)Class.forName(beanDefinition.getBeanClassName());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
        ).filter(aClass -> aClass != null).collect(Collectors.toList());
    }

    private static void mapClasses(List<? extends Class<CommandInvoke>> classes) {
        classes.stream().forEach(o -> {
            try {
                CommandInvoke instance = o.newInstance();
                name2class.put(instance.getName(), o);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }
}
