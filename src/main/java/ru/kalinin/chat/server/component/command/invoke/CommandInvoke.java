package ru.kalinin.chat.server.component.command.invoke;

import lombok.Getter;
import lombok.Setter;
import ru.kalinin.chat.server.component.command.CommandResult;

/**
 * Created by kalinin on 03.10.2017.
 */
public interface CommandInvoke {

    void setFrom(String from);
    String getFrom();
    String getHelp();
    String getName();
    void setArgs(String... args);
    CommandResult invoke();

}
