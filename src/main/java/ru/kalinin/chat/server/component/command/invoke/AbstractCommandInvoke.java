package ru.kalinin.chat.server.component.command.invoke;

import ru.kalinin.chat.server.component.command.CommandResult;

/**
 * Created by kalinin on 11.10.2017.
 */
public abstract class AbstractCommandInvoke implements CommandInvoke {

    private String from;

    @Override
    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public void setArgs(String... args) {

    }
}
