package ru.kalinin.chat.server.component.command.invoke;

import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.component.command.CommandInvokeStatus;
import ru.kalinin.chat.server.component.command.CommandResult;

/**
 * Created by kalinin on 11.10.2017.
 */
@Component
public class FakeCommand extends AbstractCommandInvoke {

    private String[] args = null;

    @Override
    public String getHelp() {
        return "это фэйк, можно задать ему параметры";
    }

    public String getName() {
        return "fake";
    }

    @Override
    public void setArgs(String... args) {
        this.args = args;
    }

    @Override
    public CommandResult invoke() {
        String argsMessage = args.length > 0 ? " \"" + String.join(", ", this.args) + "\"": "";
        return CommandResult.builder()
                .status(CommandInvokeStatus.SUCCESS)
                .message("Это фэйк с аргументами" + argsMessage)
                .build();
    }
}
