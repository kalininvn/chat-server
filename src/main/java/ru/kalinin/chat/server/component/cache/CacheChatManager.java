package ru.kalinin.chat.server.component.cache;

import org.springframework.cache.CacheManager;

import java.util.List;

/**
 * Created by kalinin on 03.10.2017.
 */
public interface CacheChatManager extends CacheManager {

    <E> void addRingbuffer(String name, E object);
    <E> List<E> getRingbuffer(String name, int maxCount, Class<E> type);
    <E> List<E> getRingbuffer(String name, Class<E> type);
}
