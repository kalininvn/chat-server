package ru.kalinin.chat.server;

/**
 * Created by kalinin on 29.09.2017.
 */
public class Constant {

    public final static String USER_NAME_SERVER = "Сервер-";

    public final static String CACHE_NAME_TOP_LATEST_MESSAGES = "ru.kalinin.chat.server.topLatestMessages";
    /**
     * DirectExchange, очередь для всех входящих сообщении серверу
     */
    public final static String QUEUE_MESSAGE_INCOMING = "ru.kalinin.chat.server.queue.message.incoming";
    /**
     * DirectExchange, точка доступа для всех входящих сообщении серверу
     */
    public final static String EXCHANGE_MESSAGE_INCOMING = "ru.kalinin.chat.server.exchange.message.incoming";
    /**
     * DirectExchange, маршрут для всех входящих сообщении серверу
     */
    public final static String ROUTING_MESSAGE_INCOMING = "ru.kalinin.chat.server.routing.message.incoming";
    /**
     * DirectExchange, очередь ответов на запрос пользователя, в нашем случае команд
     */
    public final static String QUEUE_MESSAGE_OUTGOING = "ru.kalinin.chat.server.queue.message.outgoing";
    /**
     * DirectExchange, точка доступа ответов на запрос пользователя, в нашем случае команд
     */
    public final static String EXCHANGE_MESSAGE_OUTGOING = "ru.kalinin.chat.server.exchange.message.outgoing";
    /**
     * DirectExchange, маршрут ответов на запрос пользователя, в нашем случае команд
     */
    public final static String ROUTING_MESSAGE_OUTGOING = "ru.kalinin.chat.server.routing.message.outgoing";
}
