package ru.kalinin.chat.server.controller;

import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kalinin.chat.server.helper.MessageHelper;
import ru.kalinin.chat.server.service.broker.BrokerUserManagerImpl;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

/**
 * Created by kalinin on 29.09.2017.
 */
@RestController
public class AdminController {

    @Autowired
    private RabbitAdmin adminAmqp;

    @Autowired
    private UserDetailsManager userDetailsManager;
    @Autowired
    private MessageHelper messageHelper;

//    @RequestMapping(value="/api/admin/deleteExchange", method = {RequestMethod.GET, RequestMethod.DELETE})
//    public void deleteExchange() {
//        adminAmqp.deleteExchange(messageHelper.getExchangeNameMessageIncoming());
//        adminAmqp.deleteExchange(messageHelper.getExchangeNameMessageOutgoing());
//    }

    @RequestMapping(value="/api/admin/logoutAll", method = {RequestMethod.GET})
    public void logoutAll() {
        userDetailsManager.logoutAll();
    }
}
