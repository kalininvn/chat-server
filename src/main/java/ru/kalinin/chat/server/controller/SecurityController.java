package ru.kalinin.chat.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kalinin.chat.server.producer.MessageOutgoingProducer;
import ru.kalinin.chat.server.model.client.UserInfo;
import ru.kalinin.chat.server.model.client.AuthUserImpl;
import ru.kalinin.chat.commons.model.packet.Message;
import ru.kalinin.chat.server.model.security.UserDetails;
import ru.kalinin.chat.server.service.topmessage.TopMessageService;
import ru.kalinin.chat.server.service.security.UserDetailsManager;

import javax.validation.Valid;

/**
 * Created by kalinin on 29.09.2017.
 */
@RestController
public class SecurityController {

    @Autowired
    private UserDetailsManager userDetailsManager;

    @Autowired
    private MessageOutgoingProducer messageOutgoingProducer;

    @Autowired
    private TopMessageService<Message> topMessageService;

    @RequestMapping(value="/api/authenitcation/createuser", method = RequestMethod.POST)
    public UserInfo createUser(@Valid @RequestBody AuthUserImpl authUser) {
        UserDetails userDetails = userDetailsManager.createUser(authUser);
        for (Message message : topMessageService.getLast()) {
            messageOutgoingProducer.sendUser(userDetails.getUsername(), message);
        };
        UserInfo ui = new UserInfo();
        ui.setUsername(userDetails.getUsername());
        ui.setAmqpConfiguration(userDetails.getAmqpConfiguration());
        return ui;
    }
}
