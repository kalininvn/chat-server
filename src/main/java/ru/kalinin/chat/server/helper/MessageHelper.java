package ru.kalinin.chat.server.helper;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kalinin.chat.server.Constant;

/**
 * Created by kalinin on 04.10.2017.
 */
@Component
public class MessageHelper {

    @Autowired
    private DirectExchange exchangeMessageIncoming;

    @Autowired
    private Queue queueMessageIncoming;

    @Autowired
    private TopicExchange exchangeMessageOutgoing;

    public String getExchangeNameMessageOutgoing() {
        return exchangeMessageOutgoing.getName();
    }

    public String getRoutingNameMessageOutgoing(Integer userId) {
        return Constant.ROUTING_MESSAGE_OUTGOING + ".user." + userId;
    }

    public String getRoutingNameMessageOutgoingAll() {
        return Constant.ROUTING_MESSAGE_OUTGOING + ".user.all";
    }

    public String getExchangeNameMessageIncoming() {
        return exchangeMessageIncoming.getName();
    }

    public String getQueueNameMessageIncoming() {
        return queueMessageIncoming.getName();
    }

    public String getRoutingNameMessageIncoming() {
        return Constant.ROUTING_MESSAGE_INCOMING;
    }

    public String getQueueNameMessageOutgoing(Integer userId) {
        return Constant.QUEUE_MESSAGE_OUTGOING + ".user." + userId;
    }
}
