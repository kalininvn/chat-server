package ru.kalinin.chat.server.model.security;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ru.kalinin.chat.server.model.client.AmqpConfiguration;

/**
 * Created by kalinin on 29.09.2017.
 */
public interface UserDetails {

    Integer getId();
    String getUsername();
    AmqpConfiguration getAmqpConfiguration();
}
