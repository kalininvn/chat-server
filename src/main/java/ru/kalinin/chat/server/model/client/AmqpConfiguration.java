package ru.kalinin.chat.server.model.client;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kalinin on 29.09.2017.
 */
public class AmqpConfiguration {

    /**
     * хост
     */
    public static final String HOSTNAME = "hostname";

    /**
     * порт
     */
    public static final String PORT = "port";

    /**
     * DirectExchange, точка доступа для всех входящих сообщении серверу
     */
    public static final String EXCHANGE_MESSAGE_INCOMING = "exchangeMessageIncoming";

    /**
     * Маршрут для всех входящих сообщении серверу
     */
    public static final String ROUTING_MESSAGE_INCOMING = "routingMessageIncoming";
    /**
     * Очередь для всех входящих сообщении серверу
     */
    public static final String QUEUE_MESSAGE_INCOMING = "queueMessageIncoming";
    /**
     * DirectExchange, точка доступа ответов на запрос пользователя, в нашем случае команд
     */
    public static final String EXCHANGE_MESSAGE_OUTGOING = "exchangeMessageOutgoing";

    /**
     * Очередь ответов на запрос пользователя, в нашем случае команд
     */
    public static final String QUEUE_MESSAGE_OUTGOING = "queueMessageOutgoing";

    private Properties properties = new Properties();

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
