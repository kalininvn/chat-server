package ru.kalinin.chat.server.model.security;

import lombok.*;
import ru.kalinin.chat.server.model.client.AmqpConfiguration;

/**
 * Created by kalinin on 29.09.2017.
 */
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailsImpl implements UserDetails {
    private Integer id;
    private String username;
    private AmqpConfiguration amqpConfiguration;
}
