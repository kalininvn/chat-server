package ru.kalinin.chat.server.model.client;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by kalinin on 29.09.2017.
 */
@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthUserImpl implements AuthUser {
    @NotEmpty
    private String username;
}
