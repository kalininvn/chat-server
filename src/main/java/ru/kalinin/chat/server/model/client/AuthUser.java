package ru.kalinin.chat.server.model.client;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by kalinin on 29.09.2017.
 */
public interface AuthUser {

    String getUsername();
}
