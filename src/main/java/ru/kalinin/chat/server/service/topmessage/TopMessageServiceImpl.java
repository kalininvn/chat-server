package ru.kalinin.chat.server.service.topmessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.kalinin.chat.server.Constant;
import ru.kalinin.chat.server.component.cache.CacheChatManager;
import ru.kalinin.chat.commons.model.packet.Message;

import java.util.List;

/**
 * Created by kalinin on 30.09.2017.
 */
@Service
public class TopMessageServiceImpl implements TopMessageService<Message> {

    @Value(value = "${app.config.topLatestMessages}")
    private Integer topLatestMessages;

    @Autowired
    private CacheChatManager cacheChatManager;

    @Override
    public void add(Message message) {
        cacheChatManager.addRingbuffer(Constant.CACHE_NAME_TOP_LATEST_MESSAGES, message);
    }

    @Override
    public List<Message> getLast() {
        return cacheChatManager.getRingbuffer(Constant.CACHE_NAME_TOP_LATEST_MESSAGES, Message.class);
    }
}
