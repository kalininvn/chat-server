package ru.kalinin.chat.server.service.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kalinin.chat.server.CacheNames;
import ru.kalinin.chat.server.Constant;
import ru.kalinin.chat.server.component.cache.CacheChatManager;
import ru.kalinin.chat.server.exception.AuthException;
import ru.kalinin.chat.server.exception.UserIsExistsException;
import ru.kalinin.chat.server.exception.UserLongNameException;
import ru.kalinin.chat.server.exception.UserShortNameException;
import ru.kalinin.chat.server.model.client.AmqpConfiguration;
import ru.kalinin.chat.server.model.jpa.Customer;
import ru.kalinin.chat.server.model.client.AuthUser;
import ru.kalinin.chat.server.model.security.UserDetails;
import ru.kalinin.chat.server.model.security.UserDetailsImpl;
import ru.kalinin.chat.server.repository.jpa.CustomerRepository;
import ru.kalinin.chat.server.service.broker.BrokerUserManager;

/**
 * Created by kalinin on 29.09.2017.
 */
@Service
@Slf4j
public class UserDetailsManagerImpl implements UserDetailsManager {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BrokerUserManager brokerUserManager;
    @Autowired
    private CacheChatManager cacheManager;

    @Transactional
    @Override
    public UserDetails createUser(AuthUser authUser) {
        if(authUser == null || authUser.getUsername() == null) {
            throw new AuthException();
        }
        String username = authUser.getUsername().trim();
        if(userExists(username) || username.equals(Constant.USER_NAME_SERVER)) {
            throw new UserIsExistsException();
        }
        if(username.length() < 3) {
            throw new UserShortNameException();
        }
        if(username.length() > 20) {
            throw new UserLongNameException();
        }

        Customer customer = customerRepository.save(Customer.builder()
                .name(username)
                .build()
        );
        Integer userId = customer.getId();
        AmqpConfiguration amqpConfiguration = brokerUserManager.createAndBind(userId);
        UserDetails userDetails = UserDetailsImpl.builder()
                .id(userId)
                .username(username)
                .amqpConfiguration(amqpConfiguration)
                .build();
        return userDetails;
    }

    @Cacheable(CacheNames.USER_EXISTS)
    @Override
    public boolean userExists(String username) {
        return customerRepository.countByName(username) > 0;
    }

    @CacheEvict(CacheNames.CUSTOMER_BY_NAME)
    @Override
    @Transactional
    public void deleteUserByUsername(String username) {
        customerRepository.deleteByName(username);
    }

    @Transactional
    @Override
    public void logout(String username) {
        Integer userId = null;
        if(userExists(username)) {
            userId = getUserIdByUsername(username);
            deleteUserByUsername(username);
        }
        if(userId != null) {
            brokerUserManager.leaveUser(userId);
        }
        cacheManager.getCache(CacheNames.USER_EXISTS).evict(username);
    }

    @CacheEvict(value = CacheNames.CUSTOMER_BY_NAME, allEntries = true)
    @Transactional
    @Override
    public void logoutAll() {
        for (Customer customer : customerRepository.findAll()) {
            logout(customer.getName());
        }
        cacheManager.getCache(CacheNames.USER_EXISTS).clear();
    }

    @Override
    public String getUsernameByUserId(Integer userId) {
        return customerRepository.findOne(userId).getName();
    }

    @Cacheable(CacheNames.CUSTOMER_BY_NAME)
    @Override
    public Integer getUserIdByUsername(String username) {
        return customerRepository.findByName(username).getId();
    }
}
