package ru.kalinin.chat.server.service.broker;

import ru.kalinin.chat.server.model.client.AmqpConfiguration;

/**
 * Created by kalinin on 29.09.2017.
 */
public interface BrokerUserManager {

    AmqpConfiguration createAndBind(Integer userId);
    void leaveUser(Integer userId);
}
