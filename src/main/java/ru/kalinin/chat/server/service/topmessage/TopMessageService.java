package ru.kalinin.chat.server.service.topmessage;

import java.util.List;

/**
 * Created by kalinin on 30.09.2017.
 */
public interface TopMessageService<T> {

    void add(T message);
    List<T> getLast();
}
