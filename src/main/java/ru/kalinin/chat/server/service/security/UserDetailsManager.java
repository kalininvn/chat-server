package ru.kalinin.chat.server.service.security;

import ru.kalinin.chat.server.model.client.AuthUser;
import ru.kalinin.chat.server.model.security.UserDetails;

/**
 * Created by kalinin on 29.09.2017.
 */
public interface UserDetailsManager {
    UserDetails createUser(AuthUser authUser);
    boolean userExists(String username);
    void deleteUserByUsername(String username);
    void logout(String username);
    void logoutAll();
    String getUsernameByUserId(Integer userId);
    Integer getUserIdByUsername(String username);

}
