package ru.kalinin.chat.server.service.broker;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.kalinin.chat.server.Profiles;
import ru.kalinin.chat.server.helper.MessageHelper;
import ru.kalinin.chat.server.model.client.AmqpConfiguration;

import java.util.Arrays;

/**
 * Created by kalinin on 29.09.2017.
 */
@Service
public class BrokerUserManagerImpl implements BrokerUserManager {

    @Autowired
    private RabbitAdmin adminAmqp;

    @Autowired
    private TopicExchange exchangeMessageOutgoing;

    @Autowired
    private MessageHelper messageHelper;

    @Autowired
    private Environment environment;

    public AmqpConfiguration createAndBind(Integer userId) {
        AmqpConfiguration amqpConfiguration = new AmqpConfiguration();
        amqpConfiguration.setProperty(AmqpConfiguration.HOSTNAME, adminAmqp.getRabbitTemplate().getConnectionFactory().getHost());
        amqpConfiguration.setProperty(AmqpConfiguration.PORT, String.valueOf(adminAmqp.getRabbitTemplate().getConnectionFactory().getPort()));
        amqpConfiguration.setProperty(AmqpConfiguration.EXCHANGE_MESSAGE_INCOMING, messageHelper.getExchangeNameMessageIncoming());
        amqpConfiguration.setProperty(AmqpConfiguration.QUEUE_MESSAGE_INCOMING, messageHelper.getQueueNameMessageIncoming());
        amqpConfiguration.setProperty(AmqpConfiguration.ROUTING_MESSAGE_INCOMING, messageHelper.getRoutingNameMessageIncoming());
        amqpConfiguration.setProperty(AmqpConfiguration.EXCHANGE_MESSAGE_OUTGOING, messageHelper.getExchangeNameMessageOutgoing());
        amqpConfiguration.setProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING, messageHelper.getQueueNameMessageOutgoing(userId));


        // пользователь не имеет права сам создавать очереди, политика безопасности запрещает ему
        // конкретные сообщения, команда
        adminAmqp.deleteQueue(amqpConfiguration.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        Queue queueMessageOutgoing = createQueueMessageOutgoing(amqpConfiguration.getProperty(AmqpConfiguration.QUEUE_MESSAGE_OUTGOING));
        adminAmqp.declareQueue(queueMessageOutgoing);
        adminAmqp.declareBinding(BindingBuilder.bind(queueMessageOutgoing).to(exchangeMessageOutgoing).with(messageHelper.getRoutingNameMessageOutgoing(userId)));
        adminAmqp.declareBinding(BindingBuilder.bind(queueMessageOutgoing).to(exchangeMessageOutgoing).with(messageHelper.getRoutingNameMessageOutgoingAll()));
        return amqpConfiguration;
    }

    @Override
    public void leaveUser(Integer userId) {
        adminAmqp.deleteQueue(messageHelper.getQueueNameMessageOutgoing(userId));
    }

    public Queue createQueueMessageOutgoing(String name) {

//        Queue queueMessageOutgoing = new Queue(name, false, false, true);
        Queue queueMessageOutgoing = null;
        if(Arrays.asList(environment.getActiveProfiles()).contains(Profiles.TEST)) {
            queueMessageOutgoing = new Queue(name, false, false, false);
        } else {
            queueMessageOutgoing = new Queue(name, false, false, true);
        }
        return queueMessageOutgoing;
    }
}
